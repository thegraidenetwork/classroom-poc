<?php

require_once __DIR__ . '/vendor/autoload.php';

define('APPLICATION_NAME', 'Classroom API PHP Quickstart');
define('CREDENTIALS_PATH', '/app/.credentials/classroom.googleapis.com-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
define('SCOPES', implode(' ', [
    Google_Service_Classroom::CLASSROOM_COURSES_READONLY,
    Google_Service_Classroom::CLASSROOM_ROSTERS_READONLY,
    Google_Service_Classroom::CLASSROOM_COURSEWORK_STUDENTS_READONLY,
    Google_Service_Classroom::CLASSROOM_STUDENT_SUBMISSIONS_STUDENTS_READONLY,
    Google_Service_Drive::DRIVE_METADATA,
]));

if (php_sapi_name() != 'cli') {
    throw new Exception('This application must be run on the command line.');
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient() {
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    if (file_exists(CREDENTIALS_PATH)) {
        $accessToken = json_decode(file_get_contents(CREDENTIALS_PATH), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if(!file_exists(dirname(CREDENTIALS_PATH))) {
            mkdir(dirname(CREDENTIALS_PATH), 0700, true);
        }
        file_put_contents(CREDENTIALS_PATH, json_encode($accessToken));
        printf("Credentials saved to %s\n", CREDENTIALS_PATH);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents(CREDENTIALS_PATH, json_encode($client->getAccessToken()));
    }
    return $client;
}

// Get the API client and construct the service object.
$client = getClient();
$classroom = new Google_Service_Classroom($client);
$drive = new Google_Service_Drive($client);

// Print the first 10 courses the user has access to.
$optParams = array(
    'pageSize' => 10
);

// Get the courses
$courses = $classroom->courses->listCourses($optParams);

// Get the first course's assignments
$courseWork = $classroom->courses_courseWork->listCoursesCourseWork($courses[0]->id);

echo "ASSIGNMENTS: ".PHP_EOL;
print_r($courseWork);

// Get the student submissions to the first assignment
$studentWork = $classroom->courses_courseWork_studentSubmissions->listCoursesCourseWorkStudentSubmissions('1997306867', $courseWork[0]->id);

echo "SUBMISSIONS: ".PHP_EOL;
print_r($studentWork);

/* My test account doesn't allow google drive
// Get the files in this Google drive folder
$driveFiles = $drive->files->listFiles([
    'pageSize' => 10,
    'fields' => 'nextPageToken, files(id, name)',
])->getFiles();

echo "FILES: ".PHP_EOL;
print_r($driveFiles);
*/
