# Google Classroom POC

A simple example of using the Google Classroom API in PHP.

- Install composer: `npm run composer:install`
- Add a `client_secret.json` file to the root as instructed in the [PHP quickstart](https://developers.google.com/classroom/quickstart/php).
- Run the app: `npm run app:local:run`

This will ask you to authorize your account by going to a URL and then copying the code given.

Credentials are stored in the `.credentials/` folder, but if they timeout, you can delete the folder and start over.
